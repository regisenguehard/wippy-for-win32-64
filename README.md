# Adptation de WIPPY pour Windows #

### Automatise l'installation de WordPress sous Windows à l'aide de WP-CLI ###

* WP-CLI : http://wp-cli.org/
* Cygwin : https://www.cygwin.com/
* WampServer : http://www.wampserver.com/


### Instructions ###

MAC / Linux :

* http://wp-spread.com/tuto-wp-cli-comment-installer-et-configurer-wordpress-en-moins-dune-minute-et-en-seulement-un-clic
* https://github.com/posykrat/dfwp_install

Windows :

* http://blog.enguehard.info/installer-et-configurer-wordpress-en-quelques-secondes/