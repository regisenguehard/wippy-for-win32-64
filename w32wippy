#!/bin/bash
#
# Wippy for win32/64
# Automatise l'installation de WordPress sous Windows
#
#       Installer WordPress
#       Effacer les contenu par defaut
#       Créer des pages standards : 'Accueil', 'contact', 'Mentions légales'
#       Mettre la page d'accueil en page
#       Modifier les liens avec /%postname%/
#       Ajouter des plugins
#       Ajouter des thèmes
#
#
#
# Nécessite :
#   Wampserver
#   cygwin
#
# De @regisenguehard (regis.enguehard@gmail.com)
# Fork de @maximebj (https://bitbucket.org/maximebj/wippy-spread)
# Fork de @posykrat (https://github.com/posykrat/dfwp_install/)
#
# Comment cela fonctionne ?
# w32wippy.sh nomdudossier "Nom du site" monemail@fournisseur.fr
# $1 = premier argument : nom du dossier, nom de la base de donnée, et login
# $2 = deuxieme argument : nom du site en toute lettre
# $3 = troisième argument : adresse email de l'administrateur WordPress



# VARIABLES propre à la configuration système
# URL locale
url="http://localhost/"$1

# chemin ou installer les WordPress
rootpath="/cygdrive/c/wamp/www/"

# chemin des plugins.txt
pluginfilepath="/cygdrive/c/wamp/www/wippy-for-win32-64/plugins.txt"

# WP admin login
admin=$1

# Accès SQL
dbhost="192.168.0.235"      # Correspond à l'adresse IP de l'ordinateur
dbname=$1
dbuser="root"               # L'utilisateur par defaut root sans mot de passe, ne peut se connecter que sur le localhost ; il faut créer un utilisateur spécifique
dbpass="root"
# end VARIABLES ---



#  ==============================
#  ECHO COLORS, FUNCTIONS AND VARS
#  ==============================
bggreen='\033[42m'
bgred='\033[41m'
bold='\033[1m'
black='\033[30m'
gray='\033[37m'
normal='\033[0m'


# Jump a line
function line {
    echo " "
}

# Basic echo
function bot {
    line
    echo -e "$1 ${normal}"
}

# Error echo
function error {
    line
    echo -e "${bgred}${bold}${gray} $1 ${normal}"
}

# Success echo
function success {
    line
    echo -e "${bggreen}${bold}${gray} $1 ${normal}"
}


if [ -z "$1" ]; then
    error "Indiquer le nom du dossier"
    exit;
fi


if [ -z "$2" ]; then
    error "Indiquer le nom du site"
    exit;
fi

if [ -z "$3" ]; then
    error "Indiquer l'email de l'administrateur"
    exit;
fi


# Va au répertoire d'installer des WP
cd $rootpath
pathtoinstall="$rootpath$1"

# check if provided folder name already exists
if [ -d $1 ]; then
  error "Le dossier $1 existe déjà !"
  exit 1
fi


# bot "Création du dossier "$pathtoinstall
mkdir $pathtoinstall
cd $pathtoinstall


bot "Création du fichier de configuration wp-cli.yml"
echo "
# Configuration de wpcli
# Voir http://wp-cli.org/config/
# Les modules apaches à charger
apache_modules:
    - mod_rewrite
" >> wp-cli.yml


# bot "Téléchargement de WordPress"
wp core download --locale=fr_FR --force
wp core version


# create base configuration
bot "Lancement de la configuration"
if [ -z "$dbpass" ]; then
    wp core config --dbname="$dbname" --dbuser="$dbuser" --dbhost="$dbhost"
else
    wp core config --dbname="$dbname" --dbuser="$dbuser" --dbpass="$dbpass" --dbhost="$dbhost"
fi



# Create database
bot "Création de la base de données"
wp db create


passgen=`head -c 10 /dev/random | base64`
password=${passgen:0:10}


# launch install
bot "Lancement de l'installation"
wp core install --url=$url --title="$2" --admin_user=$admin --admin_email="$3" --admin_password=$password


# Plugins install et mise à jour
if [ -e $pluginfilepath ]; then
    bot "Installe des plugins :"
    while read line
    do
        bot "-> Plugin $line"
        wp plugin install $line --activate
    done < $pluginfilepath
fi
wp plugin update --all


# Theme install
bot "installation du thème customizr"
wp theme install customizr --activate


# thème enfant
bot "Création du thème enfant"
cd wp-content/themes/
mkdir $1
echo "/*
    Theme Name: $1
    Description: Child of customizr
    Author: Régis Enguehard
    Template: customizr
    Version: 1.0
*/" > $1/style.css


bot "Suppression des posts, comments et terms"
wp site empty --yes


# Create standard pages
bot "Création des pages habituelles : Accueil, Blog, Contact, Mentions Légales..."
wp post create --post_type=page --post_title='Accueil' --post_status=publish
wp post create --post_type=page --post_title='Blog' --post_status=publish
wp post create --post_type=page --post_title='Contact' --post_status=publish
wp post create --post_type=page --post_title='Mentions Légales' --post_status=publish


bot "Suppression "Hello dolly" et des themes de bases"
wp plugin delete hello
wp theme delete twentyfourteen
wp theme delete twentythirteen
wp theme delete twentyfifteen
wp option update blogdescription ''

# La page d'accueil est une page
# Et c'est la page qui se nomme accueil
bot "Configuration de la page accueil"
wp option update show_on_front 'page'
wp option update page_on_front $(wp post list --post_type=page --post_status=publish --posts_per_page=1 --pagename=Accueil --field=ID --format=ids)

# Permalinks to /%postname%/
bot "Activation de la structure des permaliens suivante : /%postname%/"
wp rewrite structure "/%postname%/" --hard
wp rewrite flush --hard

# Activate theme
bot "Activation du thème $1 :"
wp theme activate $1



# Finish !
success "Installation terminée !"
echo "--------------------------------------"
echo -e "Url            : $url"
echo -e "Chemin         : $pathtoinstall"
echo -e "Login          : $1"
echo -e "Mot de passe   : $password"
echo -e "Email          : $3"
echo "--------------------------------------"
